#
# Copyright (c) 2012-2014 Shailendra Singh <shailendra_01@outlook.com>

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'windows'

windows_package "#{node['piston']['mysql_installer_package_name']}" do
  source "#{node['piston']['mysql_msi_installer']}"
  action :install
end

execute 'Install Mysql CE Server' do
	command "#{node['piston']['mysql_installer_console']} --nowait --action=install --type=server --product=* --config=#{node['piston']['mysql_component_name']}:passwd=#{node['piston']['mysql_root_password']};port=#{node['piston']['mysql_port']}"
	cwd "#{node['piston']['mysql_installer_install_dir']}"
end

execute 'Create DB and Tables' do
	command "mysql -P #{node['piston']['mysql_port']} -u root -p#{node['piston']['mysql_root_password']} < create_tbl_all.sql"
	cwd "D:\\Shailendra\\git-repos\\Piston-SQL\\create"
end

execute 'Upload data' do
	command "mysql -P #{node['piston']['mysql_port']} -u root -p#{node['piston']['mysql_root_password']} < insert_tbl_all.sql"
	cwd "D:\\Shailendra\\git-repos\\Piston-SQL\\insert"
end